<?php

namespace WpPluginTemplate\Util;

/**
 * The View class can be used to render html templates that live under the
 * includes/templates directory
 */
class View {

	/** @var string */
	private $template;

	/**
	 * @param string $template A template php file under `includes/templates`
	 */
	public function __construct($template) {
		$this->template = ERPBRIDGE_DIR . "includes/templates/$template";
	}

	/**
	 * Renders the view to the page
	 *
	 * @param object $context
	 *
	 * @return void
	 */
	public function render($context) {
		echo $this->_render($context);
	}

	/**
	 * Returns the view as a string
	 *
	 * @param object $context
	 *
	 * @return string
	 */
	public function view($context) {
		return $this->_render($context);
	}

	/**
	 * @param object $context
	 *
	 * @return string
	 */
	private function _render($context) {
		$callback = function($template) {
			$vars = get_object_vars($this);
			extract($vars);
			ob_start();
			include $template;
			return ob_get_clean();
		};

		if (is_object($context)) {
			return call_user_func(
				$callback->bindTo($context),
				$this->template,
			);
		} else {
			return call_user_func(
				$callback,
				$this->template,
			);
		}
	}

}
