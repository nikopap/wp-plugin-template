<?php

namespace WpPluginTemplate\Util;

trait SingletonTrait {

	/** @var static */
	protected static $instance;

	/**
	 * @return static
	 */
	final public static function instance() {
		if (static::$instance === null) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	protected function __construct() {}

	final public function __clone() {}

	final public function __wakeup() {}

}
