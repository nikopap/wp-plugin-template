<?php

namespace WpPluginTemplate;

use WpPluginTemplate\Util\SingletonTrait;

final class Plugin {

	use SingletonTrait;

	/** @var string */
	private $plugin_name;
	/** @var string */
	private $plugin_version;

	/**
	 * Plugin initialization goes here.
	 *
	 * Add your hooks and other things that need to be initialized to this
	 * function.
	 *
	 * @return void
	 */
	public function init($plugin_name, $plugin_version) {
		$this->plugin_name = $plugin_name;
		$this->plugin_version = $plugin_version;

		add_action("init", function() {
			load_plugin_textdomain("wp-plugin-template", false, dirname(WPPLUGINTEMPLATE_BASE) . "/languages");
		});
	}

}
