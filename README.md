# WordPress plugin template

Use this as a quickstart template for your own WordPress plugins.

## Checklist

- [ ] Replace all instances of "WpPluginTemplate" with your plugin name ("PluginName")
    `find -type f -exec sed -i s/WpPluginTemplate/PluginName/g {} +`

- [ ] Replace all instances of "WPPLUGINTEMPLATE" with your plugin name ("PLUGINNAME")
    `find -type f -exec sed -i s/WPPLUGINTEMPLATE/PLUGINNAME/g {} +`

- [ ] Replace all instances of "wp-plugin-template" with your plugin slug ("plugin-name")
    `find -type f -exec sed -i s/wp-plugin-template/plugin-name/g {} +`
