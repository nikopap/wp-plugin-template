<?php

/*
 * Plugin Name: WpPluginTemplate
 * Description: My plugin
 * Version: 1.0.0
 * Author: Nikos Papadakis
 * Author URI: https://gitlab.com/nikopap/wp-plugin-template
 * Text Domain: wp-plugin-template
 * Domain Path: /languages
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 */

// If this file is called directly, abort.
if (!defined("WPINC")) {
	die();
}

define('WPPLUGINTEMPLATE_VERSION', '1.0.0');
define('WPPLUGINTEMPLATE_NAME', 'WpPluginTemplate');
define('WPPLUGINTEMPLATE_FILE', __FILE__);
define('WPPLUGINTEMPLATE_DIR', plugin_dir_path(__FILE__));
define('WPPLUGINTEMPLATE_BASE', plugin_basename(__FILE__));
define('WPPLUGINTEMPLATE_URL', plugins_url('/', __FILE__));

require WPPLUGINTEMPLATE_DIR . 'vendor/autoload.php';

// Begin execution
WpPluginTemplate\Plugin::instance()->init(WPPLUGINTEMPLATE_NAME, WPPLUGINTEMPLATE_VERSION);
